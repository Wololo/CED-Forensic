
angular.module('workshop',
[
    'ngRoute',
    'angularUtils.directives.dirPagination',
    'ngAlertify',
    'angular-loading-bar'
])
.config(function($httpProvider)
{
    $httpProvider.interceptors.push('requestService');

    if(!$httpProvider.defaults.headers.get)
    {
        $httpProvider.defaults.headers.get = {};
    }
})
.config(function($locationProvider)
{
    $locationProvider.html5Mode(false);
    $locationProvider.baseHref = '/';
});
// Voeg hier meer configuraties toe
