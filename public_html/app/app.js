
(function()
{
    // Applicatie
    addScript('app/bootstrap.js');
    addScript('app/routes.js');

    // Angular loading bar
    addScript('node_modules/angular-loading-bar/build/loading-bar.min.js');
    // Services
    addScript('app/services/AuthenticationService.js');
    addScript('app/services/RequestService.js');
    addScript('app/services/ProfielService.js');
    addScript('app/services/DossierService.js');

    // Controllers
    addScript('app/controllers/AppController.js');
    addScript('app/controllers/LoginController.js');
    addScript('app/controllers/ZoekenController.js');
    addScript('app/controllers/ProfielController.js');
    addScript('app/controllers/ViewProfielController.js');
    addScript('app/controllers/ProfielAddController.js');
    addScript('app/controllers/ProfielEditController.js');
    addScript('app/controllers/DossierController.js');
    addScript('app/controllers/DossierAddController.js');
    addScript('app/controllers/DossierEditController.js');
    addScript('app/controllers/ViewDossierController.js');
    addScript('app/controllers/ClientAddController.js');
    addScript('app/controllers/ManagementInfoController.js');
    addScript('app/controllers/ManagementRapportController.js');
    addScript('app/controllers/DossierOpbouwController.js');
    addScript('app/controllers/SchadeController.js');
    addScript('app/controllers/SchadeEditController.js');
    addScript('app/controllers/ClientController.js');
    addScript('app/controllers/ClientEditController.js');
    

    function addScript(url)
    {
        document.write('<script type="text/javascript" src="' + url + '"></script>');
    }
})();
