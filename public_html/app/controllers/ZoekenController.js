angular.module('workshop').controller('ZoekenController', function ($scope, profielService, dossierService) {
    var countCount = 0;

    var checkResponseCount = function () {
        countCount++;

        if (countCount === 2) {
            $scope.loading = false;
        }
    };

    var construct = function () {
        profielService.getAllZoeken(function (users) {
            $scope.users = users;
            checkResponseCount();
        });

        dossierService.getAll(function (files) {
            $scope.files = files;
            checkResponseCount();
        });
    };

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;
        $scope.reverse = !$scope.reverse;
    };

    construct();
});
