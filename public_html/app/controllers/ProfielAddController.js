angular.module('workshop').controller('ProfielAddController', function ($scope, profielService, alertify) {
    var construct = function () {
    if($scope.theRole == 1){
        $scope.names = ["Admin", "Beheerder", "Gebruiker"];
        $scope.selectedRol = $scope.names[2];
    } else {
        $scope.names = ["Gebruiker"];
        $scope.selectedRol = $scope.names[0];
    }
        
        $scope.change();
    };

//    $scope.register = function () {
//
//        profielService.create(
//            $scope.selectedRol, $scope.telnr,
//            $scope.voornaam, $scope.achternaam, $scope.tussenvoegsel, $scope.email, $scope.wachtwoord);
//            
//            $scope.gotoProfiel();
//    };
    
    $scope.register = function () {
        alertify.cancelBtn("Annuleren")
            .confirm("Weet u zeker dat u deze gebruiker wilt toevoegen?", function () {
                    if($scope.theRole == 1){} else {
                        $scope.selectedRol = 3;
                    }
                profielService.create(
                $scope.selectedRol, $scope.telnr,
                $scope.voornaam, $scope.achternaam, $scope.tussenvoegsel, $scope.email, $scope.wachtwoord)
                        .then(function(response) {
                            $scope.gotoProfielView(response.data);
                });
            });  
    };

    $scope.required = true;

    $scope.change = function () {
        if ($scope.selectedRol === 'Gebruiker') {
            this.checker = false;
            $scope.required = false;
        } else {
            this.checker = true;
            $scope.required = true;
        }
    };


    construct();
});
