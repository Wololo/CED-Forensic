angular.module('workshop').controller('ManagementInfoController', function ($scope, dossierService, profielService, $window) {
    var construct = function () {
        $('#dateStart').DatePicker({
            flat: true,
            date: new Date(),
            format: 'Y-m-d',
            formated: true,
            calendars: 1,
            starts: 1
        });
        $('#dateEnd').DatePicker({
            flat: true,
            date: new Date(),
            format: 'Y-m-d',
            formated: true,
            calendars: 1,
            starts: 1
        });

        //Haalt alle Opdrachtgevers op voor de select
        dossierService.getClients(function (clients) {
            $scope.clients = clients;
        });
        //Haalt alle Gebruikers op voor de select
        profielService.getAll().then(function (users) {
            $scope.users = users.data;
        });

    };
    construct();

    $scope.generate = function() {
        //START OF DATEPICKER**********
        $scope.dateStart = $('#dateStart').DatePickerGetDate('Y-m-d');
        $scope.dateEnd = $('#dateEnd').DatePickerGetDate('Y-m-d');
        //END OF DATEPICKER**********

        //START OF CLIENTS**********
        //De checkbox voor alle clients
        var allClients = $('#allClients')[0];
        $scope.totalClients = $('#totalClients').children();
        //Maakt een array aan in de scope waar de geselecteerde Clients opgeslagen worden (Met Opdrachtgever ID)
        $scope.selectedClients = new Array();
        //Index is nodig omdat als je i gebruikt in de loop (waar niet alles is geselecteerd) je lege slots in de array krijgt
        var clientIndex = 0;
        //Als allClients gechecked is
        if(allClients.checked){
            for(i = 0; i < $scope.totalClients.length; i++) {
                $scope.childCheckbox = $scope.totalClients[i].firstElementChild.firstElementChild;
                $scope.selectedClients[i] = $scope.childCheckbox.value;
                clientIndex++;
            }
        }
        else{
            //Voor elke Opdrachtgever pak je als de checkbox true is de naam / ID
            for(i = 0; i < $scope.totalClients.length; i++) {
                //Pakt per Client in de lijst de checkbox en kijkt naar de value
                $scope.childCheckbox = $scope.totalClients[i].firstElementChild.firstElementChild;
                $scope.childClient = $scope.totalClients[i].innerText;
                if($scope.childCheckbox.checked){
                    // console.log($scope.childClient + " is Selected (ID = " + $scope.childCheckbox.value + ")");
                    // $scope.selectedClients[clientIndex] = $scope.childClient;
                    $scope.selectedClients[clientIndex] = $scope.childCheckbox.value;
                    clientIndex++;
                }
            }
        }
        //END OF CLIENTS**********

        //START OF AFDELING**********
        //De checkbox voor alle afdelingen
        var allDepartments = $('#allDepartments')[0];
        $scope.totalDepartments = $('#totalDepartments').children();
        //Maakt een array aan in de scope waar de geselecteerde Afdelingen opgeslagen worden (Met Afdeling ID)
        $scope.selectedDepartments = new Array();
        //Index is nodig omdat als je i gebruikt in de loop (waar niet alles is geselecteerd) je lege slots in de array krijgt
        var departmentIndex = 0;
        //Als alle Departments checked is
        if(allDepartments.checked){
            for(i = 0; i < $scope.totalDepartments.length; i++) {
                $scope.childCheckbox = $scope.totalDepartments[i].firstElementChild.firstElementChild;
                $scope.selectedDepartments[i] = $scope.childCheckbox.value;
                departmentIndex++
            }
        }
        else{
            //Voor elke Afdeling pak je als de checkbox true is de naam / ID
            for(i = 0; i < $scope.totalDepartments.length; i++) {
                //Pakt per Afdeling in de lijst de checkbox en kijkt naar de value
                $scope.childCheckbox = $scope.totalDepartments[i].firstElementChild.firstElementChild;
                $scope.childDepartment = $scope.totalDepartments[i].innerText;
                if($scope.childCheckbox.checked){
                    // console.log($scope.childDepartment + " is Selected (ID = " + $scope.childCheckbox.value + ")");
                    // $scope.selectedDepartments[departmentIndex] = $scope.childDepartment;
                    $scope.selectedDepartments[departmentIndex] = $scope.childCheckbox.value;
                    departmentIndex++;
                }
            }
        }
        //END OF AFDELING**********

        //START OF KWALIFICATIE
        $scope.qualification = $('#qualification').children();
        //Maakt een array aan in de scope waar de geselecteerde Kwalificaties opgeslagen worden (Met Kwalificatie ID)
        $scope.selectedQualification = 0;
        for(i = 0; i < $scope.qualification.length; i++) {
            //Pakt per Client in de lijst de checkbox en kijkt naar de value
            $scope.childCheckbox = $scope.qualification[i].firstElementChild.firstElementChild;
            //Als de check langs de radio button komt die checked is stopt hij de ID weg in de scope
            if($scope.childCheckbox.checked){
                $scope.selectedQualification = parseInt($scope.childCheckbox.value);
            }
        }
        if($scope.selectedQualification !== 0){

        }else{

        }
        //END OF KWALIFICATIE

        //START OF FORENSISCH EXPERT
        $scope.childSelect = $('#forensic').children().children()[0];
        var selectedForensic = $scope.childSelect.selectedOptions[0];
        if(selectedForensic.value != "?"){
            $scope.selectedForensic = selectedForensic.value;
        }
        else{
            $scope.selectedForensic = null;
        }
        //END OF FORENSISCH EXPERT
        dossierService.getManagementInfo(function(responses){
            $window.managementRapport = responses;
            $window.dateStart = $scope.dateStart;
            $window.dateEnd = $scope.dateEnd;
            $window.selectedForensic = selectedForensic.label
            $scope.gotoManagementRapport();
        },$scope);
    };
});
