angular.module('workshop').controller('ManagementRapportController', function ($scope, $window) {
    var construct = function () {
        $scope.files = $window.managementRapport;
        console.log($scope.files);

        if($scope.files === undefined || $scope.files.length === 0){
            alert("Er zijn geen dossiers gevonden voor de opgegeven criteria. Heb je misschien de pagina ververst? Je wordt teruggestuurd naar ManagementInfo en probeer het opnieuw.");
            $scope.gotoManagementInfo();
        } else {



            $scope.dateStart = $window.dateStart;
            $scope.dateEnd = $window.dateEnd;
            $scope.selectedForensic = $window.selectedForensic;
            console.log("Selected forensic: " + $scope.selectedForensic);

            if($scope.selectedForensic != ""){
                document.getElementById("forensicP").innerHTML = "De weergegeven resultaten zijn voor de geselecteerde Forensic: <b>" + $scope.selectedForensic + "</b>";
            }else{
                document.getElementById("forensicP").innerHTML = "Er is GEEN Forensic geselecteerd";
            }


            var dossierAmount = $scope.files.length;

            var invoiceAmount = 0;
            var savedAmount = 0;

            var statusNieuw = 0;
            var statusWachtenEXP = 0;
            var statusWachtenOG = 0;
            var statusAfgehandeld = 0;

            var kwalificatie1 = 0;
            var kwalificatie2 = 0;
            var kwalificatie3 = 0;

            for (i = 0; i < dossierAmount; i++) {
                var currentDossier = $scope.files[i];
                console.log(currentDossier);

                invoiceAmount += $scope.files[i].forensic.factuur_bedrag;
                savedAmount += $scope.files[i].forensic.besparing;
                if(currentDossier.status_id != 0) {
                    if (currentDossier.status_id == 1) {
                        statusNieuw++;
                    }
                    if (currentDossier.status_id == 2) {
                        statusWachtenEXP++;
                    }
                    if (currentDossier.status_id == 3) {
                        statusWachtenOG++;
                    }
                    if (currentDossier.status_id == 4) {
                        statusAfgehandeld++;
                    }
                }
                if(currentDossier.kwalificatie != 0) {
                    if(currentDossier.kwalificatie == 1) {
                        kwalificatie1++
                    }
                    if(currentDossier.kwalificatie == 2) {
                        kwalificatie2++
                    }
                    if(currentDossier.kwalificatie == 3) {
                        kwalificatie3++
                    }
                }

            }
            $scope.totalInvoice = invoiceAmount;
            $scope.totalSaved = savedAmount;

            $scope.statusNieuw = statusNieuw;
            $scope.statusWachtenEXP = statusWachtenEXP;
            $scope.statusWachtenOG = statusWachtenOG;
            $scope.statusAfgehandeld = statusAfgehandeld;

            $scope.kwalificatie1 = kwalificatie1;
            $scope.kwalificatie2 = kwalificatie2;
            $scope.kwalificatie3 = kwalificatie3;
        }
    };


    construct();

    $( "#managementToggle" ).click(function() {
        $( "#managementTable" ).slideToggle( "fast", function() {
            // Animation complete.
        });
    });
});