angular.module('workshop').controller('ClientController', function ($scope, dossierService, alertify) {
    var construct = function () {
        dossierService.getClients(function(responses){
            $scope.clients = responses;
            console.log($scope.clients);
        });
        
    };
    $scope.removeClient = function(id){
        alertify.cancelBtn("Annuleren")
            .confirm("Weet u zeker dat u deze opdrachtgever wilt verwijderen?", function () {
        dossierService.removeClient(function(responses){
            if(responses === 0){
                alert('niet verwijderd, deze opdrachtgever is gelinked aan een dossier!')
            } else {
                construct();
            }
        },id);
    });
    };
    construct();
});