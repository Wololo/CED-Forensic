/**
 * Created by Benjamin on 05/07/2017.
 */
angular.module('workshop').controller('SchadeEditController', function ($scope, dossierService, $routeParams, alertify) {

    var construct = function () {
        dossierService.getSoortSchadeSingle(function (responses) {
            $scope.schade = responses;
            console.log($scope.schade);
        }, $routeParams.id);
       
    };
    construct();
    $scope.editdamage = function () {
        alertify.cancelBtn("Annuleren")
            .confirm("Weet u zeker dat u deze schade wilt wijzigen?", function () {
                dossierService.updateSchade($scope.schade.id, $scope.schade.naam,
                    function (responses) {
                        $scope.gotoDamage();
                    });

            }, function () {
            });
    };
});