angular.module('workshop').controller('SchadeController', function ($scope, dossierService, alertify) {
    var construct = function () {
        dossierService.getSoortSchade(function(responses){
            $scope.soortenProperty = responses;
            console.log($scope.soortenProperty);
        },1);
        dossierService.getSoortSchade(function(responses){
            $scope.soortenMobility = responses;
            console.log($scope.soortenMobility);
        },2);
        dossierService.getSoortSchade(function(responses){
            $scope.soortenVitality = responses;
            console.log($scope.soortenVitality);
        },3);
    };
    construct();
    $scope.remove = function(id){
            alertify.cancelBtn("Annuleren")
            .confirm("Weet u zeker dat u deze schade wilt verwijderen?", function () {
                dossierService.removeSchade(id,
                    function (responses) {
                        if(responses === 0){
                            alert('niet verwijderd, deze schade is gelinked aan een dossier!');
                        } else {
                            construct();
                        }
                    });

            }, function () {
            });
    }
    $scope.addDamage = function(){
        dossierService.addSchade(function(responses){
            $scope.gotoDamage();
        },$scope);
    };
});