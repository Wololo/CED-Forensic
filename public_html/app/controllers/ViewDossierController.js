angular.module('workshop').controller('ViewDossierController', function ($scope, dossierService, profielService, alertify, $routeParams) {
    var construct = function () {
        dossierService.getDossier(function (files) {
            $scope.files = files;
            console.log($scope.files);
            if($scope.files.forensic.id > 0){
                profielService.getUser(function (user) {
                    
                    $scope.files.forensic.onderzoeker_naam = user.voornaam + ' ' + user.achternaam;
                }, $scope.files.forensic.onderzoeker_id);  
            }
        }, $routeParams.id);

        
    };
    construct();

    //GERBEN - Change Dossier Status Selector
    // $scope.itemList = [];
    $scope.statusTemplates = [{id:1,name:"Nieuw"},{id:2,name:"Wachten EXP"},{id:3,name:"Wachten OG"},{id:4,name:"Afgehandeld"}];

    $scope.changedStatusValue = function(fileId, item) {

        console.log("scope.changedStatusValue in ViewDossierController op DossierID: " + fileId);
        console.log(item);

        statusId = item.id;
        dossierService.setStatus(fileId, statusId, function () {$scope.gotoDossierArchive()});

    }


    //GERBEN - Recover & Archive Dossier functions
    $scope.recoverDossier = function(fileId){
        dossierService.recover(fileId, function () {$scope.gotoDossier()});
    }
    $scope.archiveDossier = function(fileId){
        dossierService.archive(fileId, function () {$scope.gotoDossierArchive()});
    }

});
