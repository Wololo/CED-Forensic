angular.module('workshop').controller('LoginController', function ($scope, authenticationService, profielService) {
    
    $scope.login = function () {
        
        authenticationService.createAuthentication($scope.email, $scope.password);

        profielService.authenticate(function (authenticator) {
            authenticationService.setAuthenticator(authenticator);
            authenticationService.storeAuthentication($scope.remember);
        });

    };
});
