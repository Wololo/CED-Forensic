angular.module('workshop').controller('DossierEditController', function ($scope, dossierService, profielService, alertify, $routeParams) {
    var construct = function () {

        dossierService.getDossier(function (files) {
            $scope.files = files;

            hideMobility();
            hideProperty();
            hideVitality();
            switch ($scope.files.afdeling.type) {
                case 1:
                    showProperty();
                    break;
                case 2:
                    showMobility();
                    break;
                case 3:
                    showVitality();
                    break;
                default:
                    alert('noty');
                    break;
            }


        dossierService.getStatus(function(states){
            $scope.states = [{
                "states" : states,
                "selected" : $scope.files.status
            }];
        });
        
        dossierService.getSoortSchade(function(schade){
            $scope.soortSchade = [{
                "soortSchade" : schade,
                "selected" : $scope.files.schade
            }];
        },$scope.files.afdeling.type);
        
        dossierService.getClients(function (clients) {
            $scope.clients = [{
                "clients" : clients,
                "selected" : $scope.files.opdrachtgever
            }];
        });
        $scope.forensicChoice = ($scope.files.forensic.id === 0) ? 0 : 1 ;
            $scope.forensicOptions = {
              "options" : [{value : 0, name : "Nee"}, {value : 1, name : "Ja"}], 
              "selected" : {value : $scope.forensicChoice}
            };
        ($scope.files.forensic.id === 0) ? hideForensic() : showForensic();
        switch($scope.files.afdelingObjectType){
            case 1:
                $scope.files.afdelingTxt = 'Property';
                break;
            case 2:
                $scope.files.afdelingTxt = 'Mobility';
                break;
            case 3:
                $scope.files.afdelingTxt = 'Vitality';
                break;
            default:
                alert('no');
                break;
        }
        profielService.getAll().then(function (response) {
            $scope.forensics = {
                "forensics" : response.data,
                "selected" : {id : $scope.files.forensic.onderzoeker_id}
            };
            
        });
        }, $routeParams.id);
        
        
    };
    construct();
    $scope.updateMe = function(){
        $scope.files.forensic.onderzoeker_id = $scope.forensics.selected.id;
        
        switch($scope.files.afdelingObjectType){
            case 1:
                dossierService.updateProperty(function(response){
                    update();
                },$scope.files.afdeling);
                break;
            case 2:
                dossierService.updateMobility(function(response){
                    update();
                },$scope.files.afdeling);
                break;
            case 3:
                dossierService.updateVitality(function(response){
                    update();
                },$scope.files.afdeling);
                break;
            default:
                alert('Don\'t you dare');
                break;
        }
    };
    var update = function () {
        
        dossierService.update(function(response){
            console.log("we did it reddit");
        },$scope.files,$scope.states[0].selected,
        $scope.clients[0].selected, $scope.soortSchade[0].selected.id);
    };
       


                        
            
    
});
