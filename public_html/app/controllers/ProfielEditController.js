angular.module('workshop').controller('ProfielEditController', function ($scope, profielService, $routeParams, alertify) {
    var construct = function () {
        profielService.getUser(function (user) {
        if($scope.theRole == 1){
            $scope.names = ["Admin", "Beheerder", "Gebruiker"];
            $scope.selectedRol = $scope.names[user.rol - 1];
        } else {
            $scope.names = ["Gebruiker"];
            $scope.selectedRol = $scope.names[0];
        }
            $scope.user = user;
            $scope.voornaam = user.voornaam;
            $scope.tussenvoegsel = user.tussenvoegsel;
            $scope.achternaam = user.achternaam;
            $scope.telnr = user.telnr;
            $scope.email = user.email;
            $scope.wachtwoord = user.wachtwoord;
        }, $routeParams.id);
    };
 
    construct();

    $scope.edit = function () {
        alertify.cancelBtn("Annuleren")
            .confirm("Weet u zeker dat u deze gebruiker wilt wijzigen?", function () {
                
                if ($scope.wachtwoord === undefined) {
                    profielService.update($scope.user.id, $scope.selectedRol,
                        $scope.telnr, $scope.voornaam, $scope.achternaam, $scope.tussenvoegsel,
                        $scope.email, null,
                        function () {
                            $scope.gotoProfielView($scope.user.id);
                        });
                } else {
                    profielService.update($scope.user.id, $scope.selectedRol,
                        $scope.telnr, $scope.voornaam, $scope.achternaam, $scope.tussenvoegsel,
                        $scope.email, $scope.wachtwoord, 
                        function () {
                            $scope.gotoProfielView($scope.user.id);
                        });
                }
            }, function () {
            });
    };
});
