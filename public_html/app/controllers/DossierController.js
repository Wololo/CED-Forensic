angular.module('workshop').controller('DossierController', function ($window, $scope, dossierService,alertify) {
    var construct = function () {
        dossierService.getAll(function (files) {

           // $scope.theRole = $window.sessionStorage.theRole;
            $scope.files = files;
            $scope.loading = false;
            $scope.orderByField = 'nummer';
            $scope.reverseSort = false;
        });
    };
    construct();
});
