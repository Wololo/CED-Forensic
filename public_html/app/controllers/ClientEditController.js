angular.module('workshop').controller('ClientEditController', function ($scope, dossierService, $routeParams) {
    var construct = function () {
        dossierService.getClient(function(response){
            $scope.theClient = response;
        },$routeParams.id);  
    };   
    construct();
    $scope.updateClient = function () {
        dossierService.updateClient(function(){
            $scope.gotoClients();
        },$scope.theClient);
    };
});
