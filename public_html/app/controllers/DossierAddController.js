angular.module('workshop').controller('DossierAddController', 
function ($scope, dossierService) {
    construct = function() {
        dossierService.getClients(function (clients) {
            $scope.clients = clients;
        });
        dossierService.getStatus(function (status) {
           $scope.status = status;
           
        });

    }
    $scope.changePhone = function(num){
        $scope.clients.forEach(function(client){
            if(client.id == num){
                $scope.contactpersoon_telefoonnr = client.telefoonnummer;
            }
        });
    };
    $scope.startTheSequence = function(){
        switch($scope.Afdeling){
            case '1':
                dossierService.addProperty(function(response){
                    $scope.afdeling_id = response;
                    $scope.register(1);
                }, $scope.property_plaats, $scope.property_adres, 
                $scope.property_huisnummer, $scope.property_toevoeging, 
                $scope.property_type, $scope.property_postcode, 
                $scope.property_naam_verzekerde);
                break;
            case '2':
                dossierService.addMobility(function(response){
                    $scope.afdeling_id = response;
                    $scope.register(2);
                }, $scope.mobility_postcode, $scope.mobility_huisnr, 
                $scope.mobility_naam_verzekerde, $scope.mobility_kenteken);
                break;
                
            case '3':
                dossierService.addVitality(function(response){
                    $scope.afdeling_id = response;
                    $scope.register(3);
                }, $scope.vitality_slachtoffernaam, 
                $scope.vitality_beschrijving, $scope.vitality_postcode, 
                $scope.vitality_huisnr);
                break;
            default:
                alert('don\'t hack me.');
                break;
    };
};
    $scope.register = function (afdeling_type) {
        var schadeId;
        if($scope.schade){
            schadeId = $scope.schade.id;
        } else {
            schadeId = 0;
        }
        dossierService.create(function (theId) {
          $scope.gotoDossierView(theId);
        },
            $scope.status = { id: parseInt($scope.Dossierstatus)},
            $scope.opdrachtgever = {id: parseInt($scope.Opdrachtgever)},
            parseInt($scope.Dossiernummer), 
            parseInt($scope.Schade_nummer), 
            $scope.Opdrachtdatum,
            parseInt($scope.Polisnummer), 
            parseInt($scope.Kwalificatie), 
            $scope.Stratiedatum,
            $scope.afdeling = { id: parseInt($scope.afdeling_id), type: parseInt(afdeling_type)},
            $scope.expert_naam,
            $scope.expert_telefoon,
            $scope.expert_mail,
            parseInt(schadeId)
            );
            

        
    };

    $scope.changeFields = function() {
        dossierService.getSoortSchade(function(soortSchade){
            $scope.soortSchade = [{
                "soortSchade" : soortSchade
            }];
            console.log($scope.soortSchade);
        }, $scope.Afdeling);
        
    };
    construct();
});
