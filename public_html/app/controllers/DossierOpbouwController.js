angular.module('workshop').controller('DossierOpbouwController', 
function ($scope, dossierService,$routeParams) {
    var construct = function(){
        dossierService.getOpbouw(function(responses){
           $scope.opbouw = responses;
           console.log($scope.opbouw);
        },$routeParams.id);
        $scope.dossier_id = parseInt($routeParams.id);
    };
    construct();
    $scope.save = function(element){
        //$scope.file=element.files[0];
        dossierService.uploadCompanyFile(function(responses){
            $scope.goToOpbouw($routeParams.id);
        },element.files[0],$scope);
    };
    $scope.downloadIt = function (id) {
        location.href = 'http://localhost:8080/api/files/getFile/'+id;
    };
    $scope.removeOpbouw = function (id) {
        dossierService.removeOpbouw(function(responses){
            construct();
        },id);
    };
});