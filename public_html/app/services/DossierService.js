angular.module('workshop').service('dossierService', function ($http) {
    var self = this;
    self.getStatus = function (onReceived) {
        var uri = 'http://localhost:8080/api/files/states';

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };
    self.getClients = function (onReceived) {
        var uri = 'http://localhost:8080/api/files/clients';

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };
    self.getClient = function (onReceived, id) {
        var uri = 'http://localhost:8080/api/files/clients/'+id;
        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };
    self.updateClient = function (onReceived, theClient) {
        var uri = 'http://localhost:8080/api/files/client/'+theClient.id;
        var data =
            {
                id : theClient.id,
                naam:theClient.naam,
                contactpersoonnaam:theClient.contactpersoonnaam,
                telefoonnummer:theClient.telefoonnummer
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
    self.removeClient = function(onReceived, id) {
        var uri = 'http://localhost:8080/api/files/clients/'+id;

        $http.delete(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };
    self.getOpbouw = function (onReceived, id) {
        var uri = 'http://localhost:8080/api/files/dossieropbouw/'+id;

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };
    self.getAll = function (onReceived) {
        var uri = 'http://localhost:8080/api/files';

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };
    self.getSoortSchade = function(onReceived, id){
        var uri = 'http://localhost:8080/api/files/soortSchade/'+id;
        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            }); 
    };
    self.getSoortSchadeSingle = function(onReceived, id){
        var uri = 'http://localhost:8080/api/files/soortSchadeSingle/'+id;
        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            }); 
    };
    self.addSchade = function (onReceived, schade) {
        var uri = 'http://localhost:8080/api/files/damageadd';
        var data =
            {
                afdeling : schade.checked,
                naam:schade.naam
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Aanmaken mislukt: ' + message);
            });

    }
    self.updateSchade = function (id, naam, responses) {
        var uri = 'http://localhost:8080/api/files/damageedit/'+id;
        var data =
            {
                id : id,
                naam : naam
            };

        $http.post(uri, data).then(function (response) {
                responses(response.data);
            },
            function (message, status) {
                console.log('Bewerken mislukt: ' + message);
            });

    };
    self.removeSchade = function (id, responses){
        var uri = 'http://localhost:8080/api/files/damageremove/'+id;

        $http.delete(uri).then(function (response) {
                responses(response.data);
            },
            function (message, status) {
                console.log('Bewerken mislukt: ' + message);
            });
    };
    self.getDossier = function (onReceived, id) {
        var uri = 'http://localhost:8080/api/files/' + id;
        $http.get(uri).then(function (response) {
                onReceived(response.data);
                
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };
    self.addMobility = function(onReceived, mobility_postcode, mobility_huisnr,
                                mobility_naam_verzekerde, mobility_kenteken) {
        var uri = 'http://localhost:8080/api/files/mobility';
        var data =
            {
                id : 0,
                postcode:mobility_postcode,
                huisnr:mobility_huisnr,
                naam_verzekerde:mobility_naam_verzekerde,
                mobility_kenteken:mobility_kenteken,
                mobility_automaat:0
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
     self.updateMobility = function(onReceived, afdeling) {
        var uri = 'http://localhost:8080/api/files/updateMobility';
        var data =
            {
                id : afdeling.id,
                mobility_kenteken:afdeling.mobility_kenteken,
                naam_verzekerde:afdeling.naam_verzekerde,
                postcode:afdeling.postcode,
                huisnr:afdeling.huisnr
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
    self.addVitality = function(onReceived, vitality_slachtoffernaam,
                                vitality_beschrijving, vitality_postcode, 
                                vitality_huisnr) {
        var uri = 'http://localhost:8080/api/files/vitality';
        var data =
            {
                id : 0,
                vitality_slachtoffernaam:vitality_slachtoffernaam,
                vitality_beschrijving:vitality_beschrijving,
                postcode:vitality_postcode,
                huisnr:vitality_huisnr
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
     self.updateVitality = function(onReceived, afdeling) {
        var uri = 'http://localhost:8080/api/files/updateVitality';
        var data =
            {
                id : afdeling.id,
                vitality_slachtoffernaam:afdeling.vitality_slachtoffernaam,
                vitality_beschrijving:afdeling.vitality_beschrijving,
                postcode:afdeling.postcode,
                huisnr:afdeling.huisnr
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
    self.addProperty = function(onReceived, property_plaats, property_adres, 
                                property_huisnummer, property_toevoeging, 
                                property_type, property_postcode, 
                                property_naam_verzekerde) {
        var uri = 'http://localhost:8080/api/files/property';
        var data =
            {
                id : 0,
                property_plaats : property_plaats,
                property_adres : property_adres,
                property_huisnr : property_huisnummer,
                property_toevoeging : property_toevoeging,
                property_type : property_type,
                postcode : property_postcode,
                naam_verzekerde : property_naam_verzekerde
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
     self.updateProperty = function(onReceived, afdeling) {
        var uri = 'http://localhost:8080/api/files/updateProperty';
        var data =
            {
                id : afdeling.id,
                property_adres:afdeling.property_adres,
                property_huisnr:afdeling.property_huisnr,
                property_plaats:afdeling.property_plaats,
                property_toevoeging:afdeling.property_toevoeging,
                property_type:afdeling.property_type,
                naam_verzekerde:afdeling.naam_verzekerde,
                postcode:afdeling.postcode
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
    self.archive = function(id, onReceived){
        var uri = 'http://localhost:8080/api/files/' + id;

        $http.delete(uri).then(function (response) {
                onReceived(response.data);

            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };
    self.recover = function(id, onReceived){
        var uri = 'http://localhost:8080/api/files/' + id;

        $http.post(uri).then(function (response) {
                onReceived(response.data);

            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };

    self.setStatus = function(id, statusId, onReceived){

        console.log("self.setStatus naar status: " + statusId + " in DossierService op DossierID: " + id);

        var uri = 'http://localhost:8080/api/files/' + id;

        $http.post(uri).then(function (response) {
                onReceived(response.data);

            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };

    self.createClient = function (onReceived, Naam, Telefoonnummer) {
        var uri = 'http://localhost:8080/api/files/clientadd';
        var data =
            {
                naam : Naam,
                telefoonnummer : Telefoonnummer
            };
        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
    
    self.create = function (onReceived, Status, Opdrachtgever, 
    Dossiernummer, Schade_nummer, Opdrachtdatum, Polisnummer, 
    Kwalificatie, Stratiedatum, Afdeling, expert_naam, 
    expert_telefoon, expert_mail, schade) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd;
        } 
        if(mm<10){
            mm='0'+mm;
        } 
        var today = dd+'-'+mm+'-'+yyyy;
        var uri = 'http://localhost:8080/api/files';
        var data =
            {
                status : Status,
                opdrachtgever: Opdrachtgever,
                nummer : Dossiernummer,
                schadenummer : Schade_nummer,
                opdrachtdatum : Opdrachtdatum,
                polisnummer : Polisnummer,
                kwalificatie : Kwalificatie,
                nieuw_registratiedatum : today,
                stratiedatum : Stratiedatum,
                afdeling: Afdeling,
                expert_naam : expert_naam,
                expert_telefoon : expert_telefoon,
                expert_mail : expert_mail,
                soortschade_id : schade
                
            };

        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
    self.getManagementInfo = function(onReceived, theScope){
        var uri = 'http://localhost:8080/api/files/getManagement';
        var data =
            {
                afdelingen: theScope.selectedDepartments,
                opdrachtgevers: theScope.selectedClients,
                resultaten: theScope.selectedResults,
                soort_schades: theScope.selectedDamage,
                datum_start: theScope.dateStart,
                datum_eind: theScope.dateEnd,
                kwalificatie: theScope.selectedQualification,
                bespaarBedrag: theScope.selectedSavedAmount,
                factuurBedrag: theScope.selectedInvoiceAmount,
                expertId: theScope.selectedForensic
            }
        console.log(data);
        $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Opvragen mislukt: ' + message);
            });
    };
    self.update = function(onReceived, file, status, 
                            opdrachtgever, soortSchade) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd;
        } 
        if(mm<10){
            mm='0'+mm;
        } 
        var today = dd+'-'+mm+'-'+yyyy;
        var uri = 'http://localhost:8080/api/files/update';
        var data =
        {
                id : file.id,
                status : status,
                opdrachtgever: opdrachtgever,
                nummer : file.nummer,
                schadenummer : file.schadenummer,
                opdrachtdatum : file.opdrachtdatum,
                polisnummer : file.polisnummer,
                kwalificatie : file.kwalificatie,
                nieuw_registratiedatum : today,
                stratiedatum : file.stratiedatum,
                afdeling: file.afdeling,
                expert_naam : file.expert_naam,
                expert_telefoon : file.expert_telefoon,
                expert_mail : file.expert_mail,
                forensic : file.forensic,
                soortschade_id : soortSchade
             
        }
                $http.post(uri, data).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
        console.log(data);
    };
    self.addClient = function (onReceived, opdrachtgever){
        var uri = 'http://localhost:8080/api/files/clientadd';
        var data =
        {
          naam:opdrachtgever.naam,
          contactpersoonnaam: opdrachtgever.contactpersoonnaam,
          telefoonnummer:opdrachtgever.telefoonnummer
                
        }
        $http.post(uri, data).then(function (response) {
            onReceived(response.data);
            },
            function (message, status) {
                console.log('Aanmaken mislukt: ' + message);
            });
    };
    self.removeOpbouw = function(onReceived, id) {
        var uri = 'http://localhost:8080/api/files/removeOpbouw/'+id;

        $http.delete(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen mislukt: ' + message);
            });
    };
    self.getCompanyFile = function(onReceived, id) {
        var uri = 'http://localhost:8080/api/files/getFile/'+id;

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                console.log('Ophalen misxxsslukt: ' + message);
            });
    }
    self.uploadCompanyFile = function (responses, file, scope) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd;
        } 
        if(mm<10){
            mm='0'+mm;
        } 
        var today = dd+'-'+mm+'-'+yyyy;
        var uri = 'http://localhost:8080/api/files/uploadFile';

        var fd = new FormData();
            fd.append('file', file);
        var data = {
            id: scope.dossier_id,
            titel: scope.titel,
            beschrijving: scope.beschrijving,
            datum_toevoeging: today
        };
        fd.append('data', JSON.stringify(data));
        $http.post("http://localhost:8080/api/files/uploadFile", fd, {
            withCredentials: false,
            headers: {
              'Content-Type': undefined
            },
            transformRequest: angular.identity,
            params: {
              fd
            },
            responseType: "arraybuffer"
          })
          .success(function(response, status, headers, config) {
            responses(response);
            
          })
          .error(function(error, status, headers, config) {
            responses(response);

            // handle else calls
          });
  };
    
});