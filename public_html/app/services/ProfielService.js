angular.module('workshop').service('profielService', function ($window, $http, $q) {
    var self = this;

    self.authenticate = function (onSuccess) {
        var uri = 'http://localhost:8080/api/users/me';

        $http.get(uri).then(function (response) {
                $window.sessionStorage.theRole = parseInt(response.data.rol);
                console.log($window.sessionStorage);
                onSuccess(response.data);
            },
            function (message, status) {
                alert('Inloggen mislukt: ' + JSON.stringify(message, null, 4));
            });
    };
 
    self.create = function (rol, telnr, voornaam, achternaam, tussenvoegsel, email, wachtwoord) {
        
        var defer = $q.defer();
        var rolNum;

        if (rol === "Admin") {
            rolNum = 1;
        } else if (rol === "Beheerder") {
            rolNum = 2;
        } else {
            rolNum = 3;
        }

        console.log("rol = " + rol);
        console.log("telnr = " + telnr);
        console.log("voornaam = " + voornaam);
        console.log("achternaam = " + achternaam);
        console.log("email = " + email);
        console.log("wachtwoord = " + wachtwoord);

        var uri = 'http://localhost:8080/api/users';
        var data =
            {
                rol: rolNum,
                telnr: telnr,
                voornaam: voornaam,
                achternaam: achternaam,
                tussenvoegsel: tussenvoegsel,
                email: email,
                wachtwoord: wachtwoord
            };


        console.log("ProfielService Create");
        console.log(data);
        $http.post(uri, data).then(function (response) {
                defer.resolve(response);
            },
            function (message, status) {
                alert('Aanmaken mislukt: ' + message);
                defer.reject(message);
            });
            return defer.promise;
    };

    self.update = function (id, rol, telnr, voornaam, achternaam, tussenvoegsel, email, wachtwoord, responses) {
        
        var rolNum;
        if (rol === "Admin") {
            rolNum = 1;
        } else if (rol === "Beheerder") {
            rolNum = 2;
        } else {
            rolNum = 3;
        }
        

        var uri = 'http://localhost:8080/api/users/update';
        
        var data =
            {
                id: id,
                rol: rolNum,
                telnr: telnr,
                voornaam: voornaam,
                achternaam: achternaam,
                tussenvoegsel: tussenvoegsel,
                email: email,
                wachtwoord: wachtwoord
                
            };
            
            console.log("wachtwoord = " + wachtwoord);
            
            $http.post(uri, data).then(function (response) {
                responses();
            },
            function (message, status) {
                alert('Bewerken mislukt: ' + message);
            });
        };

    self.getAll = function () {
        // console.log("ProfielService.getAll");
        var defer = $q.defer();
        var uri = 'http://localhost:8080/api/users';

        $http.get(uri).then(function (response) {
                defer.resolve(response);
            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
                defer.reject(message);
            });
        return defer.promise;
    };
    

    self.getAllZoeken = function (onReceived) {
        var uri = 'http://localhost:8080/api/users/searchusers';

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Ophalen mislukt: ' + message);
            });
    };

    self.getUser = function (onReceived, id) {
        var uri = 'http://localhost:8080/api/users/' + id;

        $http.get(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                
            });
    };
    
    self.delete = function (id, onReceived) {
        var uri = 'http://localhost:8080/api/users/' + id;

        $http.delete(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Verwijderen mislukt: ' + message);
            });
    };
    self.undelete = function (id, onReceived) {
        var uri = 'http://localhost:8080/api/users/unremove/' + id;

        $http.post(uri).then(function (response) {
                onReceived(response.data);
            },
            function (message, status) {
                alert('Terugzetten mislukt: ' + message);
            });
    };
});